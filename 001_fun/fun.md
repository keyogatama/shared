# fun

```kotlin
val enterFP: (Problem) -> Idea? = { println("Hello!") }
```

## basis of this talk

![fp](001_fun/fp-gateway-drug.png)

well this is not that important

# What's important? Testing itself

and today we focus on selenium


## state of our automation

    - discouraged by selenium
    - encouraged by selenium (Page Object Model)
    - cucumber
    - actions
    ...
    - (future) -> journey & lambdanium



## functional paradigm (FP)

    - vs OOP ?

    - function as a first-class citizen

    - some kotlin syntax first

    - properties that FP have (and not exclusive to FP)
        - declarative
        - composable
        - immutable
        - lazy evaluation
        - type signature that models domain
        - pure functions
        .. a lot more I think

    - focus on 5 (and kinda 2 and 6)



## problems identified

    - we code only happy path
        - need special construct to intercept sadness
        - we cannot easily assert multiple failures 



## type signature that models domain
## what domain? the Error domain

or the happy and alternative path

    - relying on type signature
    - the Either monad, a functional programming pattern



## a lot of implementation, same api

- actions - Java 

    https://gitlab.com/byorange/qa/virgo/capitalx-ui-test/-/tree/develop/src/main/java/com/capitalx/automation/actions

- actions - written in kotlin, targets kotlin & java

    https://github.com/keychera/actions

    https://gitlab.com/byorange/qa/test/icip/-/tree/main/components/actions

- lambdanium - kotlin (also harnessing coroutine)

    https://gitlab.com/byorange/qa/test/icip/-/tree/main/components/lambdanium




## the gateway drug

https://www.youtube.com/playlist?list=PLjUtUiItGJQ0vvg_Fui0_MtgJfXgIjTR6




## future that I want to talk about

- NowTesting/Journey + lambdanium
    - abstraction of testing flow
    - FP stuff, especially Monad (that I still don't understand til now)

- mesin-eskrim
    - grpc, declaratively
    - code generation
    - also FP

